import React from 'react';

class HatForm extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            locations: [],
            location: "",
            pictureUrl: "",
            color: "",
            fabric: "",
            styleName: "",
        };
        this.handleLocationChange = this.handleLocationChange.bind(this);
        this.handlePictureUrlChange = this.handlePictureUrlChange.bind(this);
        this.handleColorChange = this.handleColorChange.bind(this);
        this.handleFabricChange = this.handleFabricChange.bind(this);
        this.handleStyleNameChange = this.handleStyleNameChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    async handleSubmit(event) {
        event.preventDefault();
        const data = {...this.state}; // Getting the form's state data
        data.picture_url = data.pictureUrl;
        delete data.pictureUrl;
        data.style_name = data.styleName
        delete data.styleName
        delete data.locations
        console.log(data);

        const hatUrl = 'http://localhost:8090/api/hats/';
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            },
        };

        const response = await fetch(hatUrl, fetchConfig);
        if (response.ok) {
            const newHat = await response.json();
            console.log(newHat);

            const cleared = {
                location: "",
                pictureUrl: "",
                color: "",
                fabric: "",
                styleName: "",
            };
            this.setState(cleared);
        }
    }

    handleStyleNameChange(event) {
        const value = event.target.value;
        this.setState({styleName: value})
    }

    handleFabricChange(event) {
        const value = event.target.value;
        this.setState({fabric: value})
    }

    handleColorChange(event) {
        const value = event.target.value;
        this.setState({color: value})
    }

    handlePictureUrlChange(event) {
        const value = event.target.value;
        this.setState({pictureUrl: value})
    }

    handleLocationChange(event) {
        const value = event.target.value;
        this.setState({location: value})
    }


    async componentDidMount() {
        // Obtain the states from the database using a fetch request
        const url = 'http://localhost:8100/api/locations/';

        const response = await fetch(url);

        // Set the state of "states" to be equal to the set of states obtained from the API
        if (response.ok) {
            const data = await response.json()
            this.setState({ locations: data.locations });
        }
    }



    render() {
        return (
            <div className="row">
                <div className="offset-3 col-6">
                    <div className="shadow p-4 mt-4">
                        <h1>Create a new Hat</h1>
                        <form onSubmit={this.handleSubmit} id="create-location-form">
                            <div className="form-floating mb-3">
                                <input onChange={this.handleStyleNameChange} placeholder="Style Name" required
                                    type="text" name="styleName" id="styleName"
                                    className="form-control" value={this.state.styleName}/>
                                <label htmlFor="styleName">Style Name</label>
                            </div>
                            <div className="form-floating mb-3">
                                <input onChange={this.handleFabricChange} placeholder="Fabric" required
                                    type="text" name="fabric" id="fabric"
                                    className="form-control" value={this.state.fabric}/>
                                <label htmlFor="fabric">Fabric</label>
                            </div>
                            <div className="form-floating mb-3">
                                <input onChange={this.handleColorChange} placeholder="Color" required
                                     type="text" name="color" id="color"
                                     className="form-control" value={this.state.color}/>
                                <label htmlFor="color">Color</label>
                            </div>
                            <div className="form-floating mb-3">
                                <input onChange={this.handlePictureUrlChange} placeholder="Picture Url"
                                    type="url" name="pictureUrl" id="pictureUrl"
                                    className="form-control" value={this.state.pictureUrl}/>
                                <label htmlFor="pictureUrl">Picture Url</label>
                            </div>
                            <div className="mb-3">
                                <select onChange={this.handleLocationChange} required id="location"
                                    name="location" className="form-select" value={this.state.location}>
                                <option value="">Choose a Location</option>
                                    {this.state.locations.map(location => {
                                        return (
                                            <option key={location.href} value={location.href}>
                                                {location.closet_name}
                                            </option>
                                        );
                                    })}
                                </select>
                            </div>
                            <button className="btn btn-primary">Create</button>
                        </form>
                    </div>
                </div>
            </div>
        )
    }
}

export default HatForm;
