import React from 'react';
import { Link } from 'react-router-dom';

class HatsList extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            hats: props.hats,
            id: null,
        }

        this.delete = this.delete.bind(this);
    }


    async delete(event) {
        const hatHref = event.target.value
        const hatUrl = `http://localhost:8090${hatHref}`;

        console.log(hatUrl)

        const fetchConfig = {
            method: "delete",
            headers: {
                'Content-Type': 'application/json',
            },
        };

        const response = await fetch(hatUrl, fetchConfig);
        if (response.ok) {
            const successMessage = await response.json() // {"deleted": true}
            console.log(successMessage)
        };

    }

    render() {
        return (
            <>
            <table className="table">
                <thead>
                <tr>
                    <th>Style Name</th>
                    <th>Fabric</th>
                    <th>Color</th>
                    <th>Closet Name</th>
                    <th>Section Number</th>
                    <th>Shelf Number</th>
                    <th></th>
                </tr>
                </thead>
                <tbody>
                {this.state.hats.map(hat => {
                    return (
                    <tr key={hat.href}>
                        <td>{hat.style_name}</td>
                        <td>{hat.fabric}</td>
                        <td>{hat.color}</td>
                        <td>{hat.location.closet_name}</td>
                        <td>{hat.location.section_number}</td>
                        <td>{hat.location.shelf_number}</td>
                        <td>
                            <button onClick={this.delete} key={hat.href} value={hat.href}>Delete</button>
                        </td>
                    </tr>
                    )
                })}
                </tbody>
            </table>
            <Link to="/hats/new">
                <button>Create Hat</button>
            </Link>
            </>
        )
    }
}

export default HatsList;
