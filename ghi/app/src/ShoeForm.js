import React from 'react';

class ShoeForm extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
        bins: [],
        bin: "",
        pictureUrl: "",
        color: "",
        manufacturer: "",
        modelName: "",
    };
    this.handleBinChange = this.handleBinChange.bind(this);
    this.handlePictureUrlChange = this.handlePictureUrlChange.bind(this);
    this.handleColorChange = this.handleColorChange.bind(this);
    this.handleManufacturerChange = this.handleManufacturerChange.bind(this);
    this.handleModelNameChange = this.handleModelNameChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);

  }
  handleBinChange(event) {
    const value = event.target.value;
    this.setState({bin: value})
  }
  handlePictureUrlChange(event) {
    const value = event.target.value;
    this.setState({pictureUrl: value})
  }
  handleColorChange(event) {
    const value = event.target.value;
    this.setState({color: value})
  }
  handleManufacturerChange(event) {
    const value = event.target.value;
    this.setState({manufacturer: value})
  }
  handleModelNameChange(event) {
    const value = event.target.value;
    this.setState({modelName: value})
  }
 
  async handleSubmit(event) {
    event.preventDefault();
    const data = {...this.state};
    data.picture_url = data.pictureUrl;
    delete data.pictureUrl;
    data.model_name = data.modelName;
    delete data.modelName;
    delete data.bins;
    console.log(data);
  
    const shoeUrl = 'http://localhost:8080/api/shoes/';
    const fetchConfig = {
      method: "post",
      body: JSON.stringify(data),
      headers: {
        'Content-Type': 'application/json',
      },
    };
    const response = await fetch(shoeUrl, fetchConfig);
    if (response.ok) {
      const newShoe = await response.json();
      console.log(newShoe);

      const cleared = {
        bin: "",
        pictureUrl: "",
        color: "",
        manufacturer: "",
        modelName: "",
      };
      this.setState(cleared);
    }
  }

    async componentDidMount() {
      const url = 'http://localhost:8100/api/bins/';
  
      const response = await fetch(url);
  
      if (response.ok) {
        const data = await response.json();
        this.setState({bins: data.bins});
  
      }
    }
    render() {
      return (
        <div className="row">
          <div className="offset-3 col-6">
            <div className="shadow p-4 mt-4">
              <h1>Create a new Shoe</h1>
              <form onSubmit={this.handleSubmit} id="create-location-form">
                <div className="form-floating mb-3">
                  <input onChange={this.handleModelNameChange} value={this.state.modelName} placeholder="Model Name" required type="text" name="modelName" id="modelName" className="form-control" />
                  <label htmlFor="name">Model Name</label>
                </div>
                <div className="form-floating mb-3">
                  <input onChange={this.handleManufacturerChange} value={this.state.manufacturer} placeholder="Manufacturer" required type="text" name="manufacturer" id="manufacturer" className="form-control" />
                  <label htmlFor="name">Manufacturer</label>
                </div>
                <div className="form-floating mb-3">
                  <input onChange={this.handleColorChange} value={this.state.color} placeholder="Color" required type="text" name="color" id="color" className="form-control" />
                  <label htmlFor="room_count">Color</label>
                </div>
                <div className="form-floating mb-3">
                  <input type="url" onChange={this.handlePictureUrlChange} value={this.state.pictureUrl} placeholder="Picture Url" name="pictureUrl" id="pictureUrl" className="form-control" />
                  <label htmlFor="city">Picture Url</label>
                </div>
                <div className="mb-3">
                <select onChange={this.handleBinChange} value={this.state.bin} required name="bin" id="bin" className="form-select">
                  <option value="">Choose a Bin</option>
                  {this.state.bins.map(bin => {
                    return (
                    <option key={bin.href} value={bin.href}>
                      {bin.closet_name}
                      </option>
                    );
                  })}
                </select>
                </div>
                <button className="btn btn-primary">Create</button>
              </form>
            </div>
          </div>
        </div>
      );
    }
  }

export default ShoeForm;