import React from 'react'
import { Link } from 'react-router-dom'

class ShoesList extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            shoes: props.shoes,
            id: null,
        }
        this.delete = this.delete.bind(this);
    }
    
async delete(event) {
    const shoeHref = event.target.value
    const shoeUrl = `http://localhost:8080${shoeHref}`;

    console.log(shoeUrl)

    const fetchConfig = {
      method: "delete",
      headers: {
        'Content-Type': 'application/json',
      },
    };

    const response = await fetch(shoeUrl, fetchConfig);
    if (response.ok) {
        const successMessage = await response.json() //("deleted": true)
        console.log(successMessage)
        };

    }

render() {
    return (
        <>
        <table className="table">
        <thead>
          <tr>
            <th>Model Name</th>
            <th>Manufacturer</th>
            <th>Color</th>
            <th>Closet Name</th>
            <th>Bin Number</th>
            <th>Bin Size</th>
            <th></th>
          </tr>
        </thead>
        <tbody>
          {this.state.shoes.map(shoe => {
            return (
              <tr key={shoe.href}>
                <td>{ shoe.model_name }</td>
                <td>{ shoe.manufacturer }</td>
                <td>{ shoe.color }</td>
                <td>{ shoe.bin.closet_name }</td>
                <td>{ shoe.bin.bin_number }</td>
                <td>{ shoe.bin.bin_size }</td>
                <td>
                <button onClick={this.delete} key={shoe.href} value={shoe.href}>Delete</button>
                </td>
              </tr>
            )
          })}
        </tbody>
      </table>
      <Link to="/shoes/new">
        <button>Create a Shoe</button>
      </Link>
      </>
    )
        }
        
        }
        export default ShoesList;