# Wardrobify

Team:

* Person 1 - Which microservice?
 * Jonathan Hats

* Person 2 - Which microservice?
    * Laurie Shoes

## QuickStart
Here are some quick tips and information to get Wardrobify up and running quickly!

Tools That We Use to Run our App:
- Docker Application
- Insomnia
- Browser
- VS Code
- CLI

Languages/Frameworks/Databases We Used:
- Python / Django Framework
- Javascript / React
- Node
- PostGresSQL / Database
- Docker
- Bootstrap


Steps to get our application running
1. Fork and Clone our repository from GitHub here: https://gitlab.com/jduriu/microservice-two-shot.git
2. Using Docker, create a PostGres Database `docker volume create pgdata`
3. Build the Docker Images `docker-compose build`
4. Build and Run the Docker Containers `docker-compose up`

At this point all containers should be running at the same time! If not, please repeat the above steps.
If all containers are running then the application has started and the following microservices can be used by both an API Client such as Insomnia or a Browser such as Google Chrome.


## Design

Here is a link to our Miro whiteboard which shows our overall Design Domain Structure! This application consists of a combination of microservices!

https://miro.com/app/board/uXjVPLgDl9Y=/

Continue reading to learn more about the individual microservices and how they work.

## Hats microservice 

Welcome to the Hats microservice of the Wardrobify Application. Have you ever thought to yourself, "Man I really need a way to organize my hat collection!" Well the Hats Microservice allows you to create, store, and organize hats! Locations can be created using the integrated Wardrobe API (see Hats Quick Start section). We hope that the following sections will help get you familiar with our microservice and on quickly on the way to hat organization freedom!

### Hats Models

Hat

The Hat model represents a hat object instance. This includes the following properties:
> - style_name: A text property relating to a specific hat object (e.g. Giants Baseball, 49ers Trucker, Warriors Fitted)
> - fabric: A text proprerty relating to the fabric used to make the hat (e.g. Cotton, Wool, Nylon, Straw)
> - color: A text property relating to the color of the hat (e.g. Black, Blue, Red, or combinations Black/Red, etc.)
> - picture_url: A url property which enables a user to enter an image url to associate with the hat object
> - location: A reference property to the Location Value Object Model (aka LocationVO). This is pulled from the Wardrobe API to specify a specific location where the hat will be stored


LocationVO

The LocationVO model is a representation of the Location model. A poller is used to obtain data from the included Wardrobe API. This poller calls every minute to check for any new locations and adds them to the Hats api model as location value objects. These location value objects can be used as a reference for creating hat instances.

> - closet_name: A text field specifying the name of a closet location
> - section_number: A number field representing a section of a closet
> - shelf_number: A number field representing a shelf number in a section of a closet


### Hats Quick Start

Here are the REST endpoints that can be utilized with an API client such as Insmonia. Along with some sample request body's for relevant endpoints.

| Request Method | URL              | What it does                      |
| -------------- | :--------------: | :-------------------------------: |
| GET            | /api/hats/       | Obtains a list of hats            |
| GET            | /api/hats/id#    | Obtains individual hat details    |
| POST           | /api/hats/       | Creates a new hat instance        |
| DELETE         | /api/hats/id#    | Deletes an instance of a hat      |

Example POST request to create a hat using a JSON body:

`
{
	"style_name": "49ers Baseball",
	"fabric": "Wool",
	"color": "Black",
	"location": "/api/locations/1/",
	"picture_url": ""
}
`

> * Note that the picture_url field is allowed to be empty if an image cannot be found at the time of creation
> * A future feature update, will allow a user to update a hat instance

- location references can be seen and created by using Http Requests to our Wardrobe API

The url for the Wardrobe API is http://localhost:8100

| GET           | /api/locations/   | Obtains a list of available locations |
| POST          | /api/locations/   | Creates a location                    |


Example POST request to create a location using a JSON body:

`
{
		"closet_name": "Narnia Closet",
		"section_number": 1,
		"shelf_number": 1
}
`

Hat model instances can also be created using the web application interface through a browser such as Google Chrome.

The url for the homepage is http://localhost:3000

- From there you can click on the Hats to see a list of already created hats
- There is a provided form page to create a hat which can be accessed using the navbar link "Create a Hat" or by pressing the "Create Hat" button at the bottom of the hats list.
- Hats can be deleted from the hats list page by pressing on the "Delete" buttons located on each row of the table
* Please note that the page will need to be refreshed after deleting to see the updated list
* Auto-updating is an intended feature for future updates.

## Shoes microservice

To all those who want to declutter the mounts of shoes stacking up in your bins... Be amazed at the new and improved Shoes microservice of the Wardrobigy application! For those who want to organize we have the perfect application for you! Create, store and organization your shoes into these easy accessible bins, created with a click of a button. Bins can be created using the integrated wardrobe api (see shoe quick start section). Happy organizing!

## Shoe Model

Shoe

The shoe model represents a hat instance. This includes the following properties
Model_name: the name of a specific shoe object [e.g. Adidas UltraBoost]
Manufacturer: a text property relating to the manufacturer to make the shoe [e.g. Crocs]
Color: a text property relating to the color of the shoe [e.g. white, red]
Picture Url: a url property which enables a user to enter an image url to associate with the shoe object
Bin: a refernece property to the bin value object model (aka BinVO). This is pulled from the wardrobe API which is used to specify the bin where the shoe will be stored.


| Request Method  | Url             | What it does                    |
| ------------- |:---------------:| :------------------------------:|
| GET           | /api/shoes/     | Obtains a list of shoes         |
| GET           | /api/shoes/id#  | Obtains individual shoe details |
| POST          | /api/shoes/     | Creates a new shoe instance     |
| DELETE        | /api/shoes/id#  | Deletes an instance of a shoe   |

Example POST request using a JSON body:

{
		"model_name": "Nike Dunk",
		"manufacturer": "Nike",
		"color": "White",
		"bin": "/api/bins/2/",
        "picture_url": ""
}

>* Note that the picture_url is empty as an image cannot be found at the time of creation 
>* Model instances can also be created using the web application interface through a browser such as Google Chrome.
>* A future feature update will allow a user to update a shoe instance
>* Bin references can be seen and created by using HTTP requests to our Wardrobe API

The url for the Wardrobe API is http://localhost:8100

| GET| /api/bins/| Obtains a list of available locations || POST | /api/bins/ | Creates a Bin |

Example POST request to create a location using a JSON body:

{ "closet_name": "New Closet", "bin_number": 1, "bin_size": 2 }

Shoe model instances can also be created using the web application interface through a browser such as Google Chrome.

The url for the homepage is http://localhost:3000

* From there you can click on the Shoes to see a list of already created shoes.
*There is a provided form page to create a hat which can be accessed using the navbar link "Create a Shoe" or by pressing the "Create Shoe" button at the bottom of the shoes list.
*Shoes can be deleted from the shoes list page by pressing on the "Delete" buttons located on each row of the table

*Please note that the page will need to be refreshed after deleting to see the updated list
*Auto-updating is an intended feature for future updates.
