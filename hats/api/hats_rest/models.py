from django.db import models
from django.urls import reverse

# Create your models here.

class LocationVO(models.Model):
    import_href = models.CharField(max_length=200, unique=True)
    closet_name = models.CharField(max_length=100)
    section_number = models.PositiveIntegerField(null=True)
    shelf_number = models.PositiveIntegerField(null=True)



class Hat(models.Model):
    """
    The Hat model represents a hat with a style, fabric, color, picture,
    and location in the wardrobe.
    """
    style_name = models.CharField(max_length=50)
    picture_url = models.URLField(blank=True, null=True)
    fabric = models.CharField(max_length=50)
    color = models.CharField(max_length=50)

    location = models.ForeignKey(
        LocationVO,
        related_name="hats",
        on_delete=models.CASCADE,
    )

    def get_api_url(self):
        return reverse("api_show_hat", kwargs={"pk": self.pk})

    def __str__(self):
        return self.name
