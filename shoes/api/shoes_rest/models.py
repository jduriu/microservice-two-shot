from django.db import models
from django.urls import reverse


class BinVO(models.Model):
    import_href = models.CharField(max_length=200, unique=True)
    closet_name = models.CharField(max_length=100, null=True)
    bin_number = models.PositiveSmallIntegerField(null=True)
    bin_size = models.PositiveSmallIntegerField(null=True)


class Shoe(models.Model):
    model_name = models.CharField(max_length=200)
    manufacturer = models.CharField(max_length=50, null=True)
    color = models.CharField(max_length=20)
    picture_url = models.URLField(blank=True, null=True)
    
    
    bin = models.ForeignKey(
        BinVO,
        related_name="bins",
        on_delete=models.CASCADE,
    )

    def get_api_url(self):
        return reverse("api_show_bin", kwargs={"pk": self.pk})

 
    def __str__(self):
        return self.name



